<div align ="center"><b>Testing Ansible Roles with Molecule</b></div>

[[_TOC_]]

## Description
The repository is an example of using Ansible along with Molecule (Docker backend) to debug written Ansible roles

For more information, please refer to the [Molecule documentation](https://molecule.readthedocs.io/en/latest/installation.html)

## System requirements
- Docker
- Python3

## Install Ansible and Molecule
    $ pip install -r requirements.txt

## Example usage
#### Test will execute the sequence necessary to test the instances
    $ cd roles/docker_ce_install
    $ molecule test

## Destroy test enviroment
    $ cd roles/docker_ce_install
    $ molecule destroy
